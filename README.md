# What is this?
A little set of ruby programs to extract 'bigkeys' and 'memkeys' output from redis-cli, parse/reformat the output to JSON, and shove it into a GCS object storage bucket, and then anaylse/report the captured information.

# Why?
gitlab.com operates with some fairly large redis instances, and we want to see what is actually in there.  We can reason based on the code, but sometimes there are bugs, or unexpected interactions, or pathological edge cases that lead to unexpected outcomes, and being able to see the size of things in redis can be enlightening.  It's far from the whole story, but finding the 'biggest' things is a reasonable start.

# Details
There are a number of gitlab.com specific assumptions built into this repo.  It's not impossible (or even necessarily difficult) to run it elsewhere, but has not been written with an eye to general re-usability.  MRs are welcome, if they don't compromise the requirements of running it on gitlab.com

The bigkeys and memkeys requests are intensive scan operations, so they shouldn't run on a primary redis instance that you care about and which is otherwise busy.  Therefore this script explicitly only runs on a replica (it quickly exits if run on a primary redis node), and has locking to ensure it only starts up once if multiple replicas start within a short window.  Scheduling is not part of this repo; for gitlab.com we have a cookbook which deploys it and a systemd timer to run it at appropriate intervals.  When using it, take care to ensure that if you have multiple replicas and only want to run on one at a time, that your scheduling is consistent, so that the locking works as expected.

In the event of a sentinel failover, the script will fall over in a screaming heap with an exception, probably from a connection error,but it does vary.  This is considered acceptable, as failovers should not be common.

# Configuration
By environment variables.  All are required:

* REDISCLI_AUTH - the redis-cli password (passed as-is, in that env variable)
* GCS_BUCKET - name of a GCS bucket to write the results to
* GOOGLE_CLOUD_PROJECT - the name of the GCP project that contains the bucket
* GOOGLE_CLOUD_CREDENTIALS - path to a JSON file that contains a GCP service account key with access to create objects in $GCS_BUCKET.  `roles/storage.objectAdmin` is sufficient, if you want to use a pre-defined role.
* FILE_PREFIX - a string to prepend to the file/object name created for each extraction.  Used because we have multiple redis instances (primary persistent, sidekiq, and cache, and potentially more in future).

The name of the object/file created is $FILE_PREFIX.%Y-%m-%dT%H%M%S

# Analysis/Viewing

In the 'report' directory is a redis-bigkey-report.rb script, which is a bare-bones utility for retrieving the JSON data recorded by redis_bigkey_extract.rb, and displaying it in a simple curses interface.  It can either read JSON files from a local directory (e.g. if you've copied to your local disk), or read directly from GCS.  In the latter case, run it from a location that has GCP credentials already configured (e.g. where you can run 'gsutil' to view the bucket)

It takes three arguments:
* -d : a local directory in which it can find a bunch of the JSON files
* -b : a GCS bucket you have access to which contains the JSON files; if -d is also present, -b takes precedence
* -p : a file prefix, equivalent to FILE_PREFIX for the extract, so you can find a specific instance of redis in a shared target bucket

One of -d or -b must be specified; -p must always be specified.

It starts by displaying the latest available report; left and right arrows navigate in time.  The JSON is automatically loaded from GCS in the background; depending on your latency, this might result in a slow responses early on, but once loaded they are cached in RAM.  Once cached, you can scroll through quite quickly to spot patterns

'q' or Ctrl-C will quit
