# frozen_string_literal: true

require 'fog/google'
require 'json'
require 'redis'

def check_required_env_vars
  %w[REDISCLI_AUTH GCS_BUCKET GOOGLE_CLOUD_PROJECT GOOGLE_CLOUD_CREDENTIALS FILE_PREFIX].each do |varname|
    raise("Must supply #{varname} environment variable") unless ENV[varname]
  end
end

def with_lock(redis, key, timeout)
  res = redis.set(key, Time.now.to_i + timeout, { nx: true, ex: timeout })
  unless res
    puts "Lock '#{key}' already held; not running"
    return
  end

  yield
ensure
  redis.del(key)
end

def extract_summary(arg)
  # redis-cli relies on REDISCLI_AUTH env var
  output_parts = `/opt/gitlab/embedded/bin/redis-cli --#{arg}`.split("-------- summary -------\n")

  raise("Could not parse redis-cli --#{arg} output; did not find 'summary' break") if output_parts.length != 2

  output_parts[1]
end

def memkeys_summary
  # -1 means sample all elements of nested keys like zsets and lists
  # It is equivalent to 0 but necessary because 0 is used by redis-cli to indicate
  # default sampling size (5, built-in to redis itself)
  extract_summary('memkeys --memkeys-samples -1')
end

def bigkeys_summary
  extract_summary('bigkeys')
end

def matches_without_type(match)
  match.named_captures.reject { |key| key == 'type' }
end

def generate_report
  result = {
    biggest: {
      by_elements: {},
      by_bytes: {}
    },
    summary: {
      by_elements: {},
      by_bytes: {}
    }
  }

  # Example: Biggest   list found 'gitlab:job_waiter:8b0063dd-d9ae-4636-bbab-c40c723dabfb' has 8867 items:
  biggest_regex =
    Regexp.new("Biggest +(?<type>[^ ]+) found '(?<key>[^']+)' has (?<elements>[0-9]+) (?<element_type>[^ ]+)")
  # Example: 10192 lists with 1633097 items (00.85% of keys, avg size 160.23)
  summary_regex =
    Regexp.new('(?<count>[0-9]+) (?<type>[^ ]+) with (?<total_elements>[0-9]+) (?<element_type>[^ ]+)')

  bigkeys_summary.split("\n").each do |line|
    if (match = biggest_regex.match(line))
      type = match[:type]
      result[:biggest][:by_elements][type] = matches_without_type(match)
    elsif (match = summary_regex.match(line))
      type = match[:type]
      result[:summary][:by_elements][type] = matches_without_type(match)
    end
  end

  memkeys_summary.split("\n").each do |line|
    if (match = biggest_regex.match(line))
      type = match[:type]
      result[:biggest][:by_bytes][type] = {
        key: match[:key],
        bytes: match[:elements]
      }
    elsif (match = summary_regex.match(line))
      type = match[:type]
      result[:summary][:by_bytes][type] = {
        count: match[:count],
        total_bytes: match[:total_elements]
      }
    end
  end

  result
end

def upload(contents)
  connection = Fog::Storage.new(
    provider: 'google',
    google_project: ENV['GOOGLE_CLOUD_PROJECT'],
    google_json_key_location: ENV['GOOGLE_CLOUD_CREDENTIALS']
  )
  filename = "bigkeys/#{ENV['FILE_PREFIX']}.#{Time.now.strftime('%Y-%m-%dT%H%M%S')}"

  connection.put_object(
    ENV['GCS_BUCKET'],
    filename,
    StringIO.new(contents.to_json)
  )
end

check_required_env_vars

role_outputs = `/opt/gitlab/embedded/bin/redis-cli --csv role`.split(',')
role = role_outputs[0].tr('"', '')
raise("Unexpected redis role '#{role}'") unless %w[master slave].include?(role)

if role == 'master'
  puts 'Not running because this is the master'
  # An acceptable case (not a fail), just exit quietly
  exit(0)
end

# Find primary and grab a time-limited lock
# Does not use sentinel because that requires a lot more knowledge of which type
# of redis we're running on (cache, sidekiq, permanent, other-in-future) to look
# up gitlab config, when on a secondary we can trivially find and use the
# primary directly
primary = role_outputs[1].tr('"', '')
redis_primary = Redis.new(
  host: primary, port: 6379, db: 0, password: ENV['REDISCLI_AUTH']
)

with_lock(redis_primary, 'redis_analysis:bigkeys:lock', 15) do
  upload(generate_report)
end
