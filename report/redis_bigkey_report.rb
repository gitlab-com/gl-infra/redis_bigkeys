#!/usr/bin/env ruby

require 'curses'
require 'json'
require 'optparse'
require 'google/cloud/storage'

ENV["GOOGLE_AUTH_SUPPRESS_CREDENTIALS_WARNINGS"]="1"

Options = Struct.new(:dir, :prefix, :bucket_name, :bucket, :preload_count)
$options = Options.new()
$options.preload_count = 200

OptionParser.new do |opts|
  opts.on('-dDIRECTORY' 'Directory to find json files in') do |d|
    $options.dir = d
  end
  opts.on('-bBUCKET' 'GCS bucket_name to find json files in') do |b|
    $options.bucket_name = b
  end
  opts.on('-pPREFIX' 'File prefix e.g. redis, redis-cache, redis-sidekiq') do |p|
    $options.prefix = p
  end
  opts.on('-nNUMBER' 'Max number of files to download/cache from GCS; optional defaults to 200') do |n|
    $options.preload_count = n.to_i
  end
end.parse!

if !($options.dir || $options.bucket_name)
  puts "Must specify either a local directory with -d, or a GCS bucket_name source with -b"
  exit
end

$bucket_cache = Hash.new do |cache, filename|
  cache[filename] = JSON.load($options.bucket.file(filename).download)
end

def display_file(filename)
  if $options.bucket
    data = $bucket_cache[filename]
  else
    file = File.open(filename)
    data = JSON.load(file)
  end

  ts_parts = File.basename(filename)[$options.prefix.length+1..].split("T")
  time = ts_parts[1]
  timestamp = "#{ts_parts[0]} #{time[0..1]}:#{time[2..3]} UTC"

  Curses.clear
  Curses.setpos(0, 0)
  Curses.addstr(timestamp)
  Curses.setpos(1, 0)
  Curses.addstr("Biggest")

  line = 2
  Curses.setpos(line, 0)
  Curses.addstr("  By elements")
  line += 1
  by_el = data["biggest"]["by_elements"]
  by_el.each do |type, details|
    Curses.setpos(line, 4)
    Curses.addstr(
      sprintf("%-6s %10d %-7s %s", type, details["elements"], details["element_type"], details["key"])
    )
    line += 1
  end

  Curses.setpos(line, 2)
  Curses.addstr("By bytes")
  line += 1
  by_bytes = data["biggest"]["by_bytes"]
  by_bytes.each do |type, details|
    Curses.setpos(line, 4)
    Curses.addstr(
      sprintf("%-6s %10d         %s", type, details["bytes"], details["key"])
    )
    line += 1
  end

  Curses.setpos(line, 0)
  Curses.addstr("Totals")
  line += 1

  Curses.setpos(line, 2)
  Curses.addstr("By elements")
  line += 1
  by_el = data["summary"]["by_elements"]
  by_el.each do |type, details|
    Curses.setpos(line, 4)
    Curses.addstr(
      sprintf("%-8s %10d %12s %s", type, details["count"], details["total_elements"], details["element_type"])
    )
    line += 1
  end

  Curses.setpos(line, 2)
  Curses.addstr("By bytes")
  line += 1
  by_bytes = data["summary"]["by_bytes"]
  by_bytes.each do |type, details|
    Curses.setpos(line, 4)
    Curses.addstr(
      sprintf("%-8s %10d %12d", type, details["count"], details["total_bytes"])
    )
    line += 1
  end

  Curses.setpos(Curses.lines - 1, 0)
  Curses.addstr("q to quit, Left/Right arrows to scroll in time")
  Curses.refresh
end

if $options.dir
  files = Dir["#{$options.dir}/#{$options.prefix}.*"].sort
elsif $options.bucket_name
  puts "Loading file list from bucket"
  storage = Google::Cloud::Storage.new
  $options.bucket = storage.bucket($options.bucket_name)
  files = $options.bucket.files(prefix:"bigkeys/#{$options.prefix}.")
    .all
    .map(&:name)
    .sort

  if files.empty?
    puts "Found no files"
    exit
  end
  puts "Got files; starting background load"

  # Start some background threads fetching the objects.  Just do what you can,
  # no need to join.   And in reverse, because we start at the end and
  # the user will usually be navigating backwards
  (0..3).each do |mod|
    Thread.new do
      files.reverse[0..$options.preload_count-1].each_with_index do |filename, index|
        next if index % 4 != mod
        foo = $bucket_cache[filename]
      end
    end
  end
end

current_index = files.size - 1
Curses.init_screen
Curses.crmode
Curses.curs_set(0)
Curses.noecho
Curses.stdscr.keypad = true

display_file(files[current_index])
finish = false

until finish do
  key = Curses.getch
  case key
  when 'r'
    Curses.clear
  when 'q'
    finish = true
  when Curses::Key::LEFT
    current_index -= 1 unless current_index == 0
  when Curses::Key::RIGHT
    current_index += 1 unless current_index >= files.size - 1
  end
  display_file(files[current_index])
end

Curses.close_screen
